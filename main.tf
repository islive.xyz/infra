terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    talos = {
      source  = "siderolabs/talos"
      version = "0.5.0"
    }
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.45"
    }
  }
  backend "http" {
  }
}

variable "do_token" {
  sensitive = true
}
variable "do_spaces_access_id" {
  sensitive = true
}
variable "do_spaces_secret_key" {
  sensitive = true
}

provider "digitalocean" {
  token             = var.do_token
  spaces_access_id  = var.do_spaces_access_id
  spaces_secret_key = var.do_spaces_secret_key
}

variable "hcloud_token" {
  sensitive = true
}

provider "hcloud" {
  token = var.hcloud_token
}

module "cluster-hcloud-fsn1-dc14-1" {
  source = "./clusters/hcloud-fsn1-dc14-1"

  providers = {
    hcloud = hcloud
  }

  hcloud_token = var.hcloud_token
}
