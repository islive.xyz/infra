locals {
  cluster_name               = "syd1-talos"
  region                     = "syd1"
  controlplane_nodes         = 2
  controlplane_node_size     = "s-4vcpu-8gb"
  talos_version              = "v1.7.2"
  talos_factory_schematic_id = "0859c09d1aaae6b1ef55af310c12c5992ec8b3992f8fd2e37afbb5dca6e52118"
  # v1.7.1
  # customization:
  #   extraKernelArgs:
  #       - console=console=ttyS1
  #       - talos.platform=digitalocean
  #   systemExtensions:
  #       officialExtensions:
  #           - siderolabs/kata-containers
  digitalocean_ccm_version = "v0.1.50"
  kubernetes_version       = "v1.30.0"
}
