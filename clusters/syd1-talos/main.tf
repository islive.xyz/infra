terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
    talos = {
      source = "siderolabs/talos"
    }
  }
}

resource "random_pet" "random" {
  count  = local.controlplane_nodes
  length = 2
}

resource "digitalocean_custom_image" "talos" {
  name    = "talos"
  url     = "https://github.com/siderolabs/talos/releases/download/${local.talos_version}/digital-ocean-amd64.raw.gz"
  regions = [local.region]
}

resource "talos_machine_secrets" "machine_secrets" {
  talos_version = local.talos_version
}

resource "digitalocean_droplet" "controlplane" {
  for_each = { for idx, val in random_pet.random : idx => val }
  image    = digitalocean_custom_image.talos.id
  name     = "${local.cluster_name}-${each.value.id}"
  region   = local.region
  size     = local.controlplane_node_size
  ssh_keys = [20632415]

  lifecycle {
    prevent_destroy = true
  }
}

resource "talos_machine_configuration_apply" "cp" {
  for_each                    = { for idx, val in digitalocean_droplet.controlplane : idx => val }
  endpoint                    = each.value.ipv4_address
  node                        = each.value.ipv4_address
  client_configuration        = talos_machine_secrets.machine_secrets.client_configuration
  machine_configuration_input = data.talos_machine_configuration.controlplane.machine_configuration
  config_patches = [
    <<-EOT
    machine:
       nodeLabels:
         failure-domain.beta.kubernetes.io/region: ${local.region}
         node.kubernetes.io/instance-type: ${local.controlplane_node_size}
         beta.kubernetes.io/instance-type: ${local.controlplane_node_size}
       kubelet:
         extraArgs:
           cloud-provider: external
       install:
         disk: /dev/vda
         extraKernelArgs:
            - console=console=ttyS1
            - talos.platform=digitalocean
         wipe: false
         image: factory.talos.dev/installer/${local.talos_factory_schematic_id}:v1.7.1
       features:
         kubePrism:
           enabled: true
           port: 7445
    cluster:
       allowSchedulingOnMasters: true
       externalCloudProvider:
         enabled: true
         manifests:
           - https://github.com/digitalocean/digitalocean-cloud-controller-manager/raw/${local.digitalocean_ccm_version}/releases/digitalocean-cloud-controller-manager/${local.digitalocean_ccm_version}.yml
       controllerManager:
         extraArgs:
           cloud-provider: external
       apiServer:
         extraArgs:
           cloud-provider: external
       inlineManifests:
         - name: do-cloud-config
           contents: |
             apiVersion: v1
             stringData:
               access-token: ${var.do_token}
             kind: Secret
             metadata:
               name: digitalocean
               namespace: kube-system
    EOT
  ]

  lifecycle {
    prevent_destroy = true
  }
}

resource "talos_machine_bootstrap" "bootstrap" {
  depends_on = [
    talos_machine_configuration_apply.cp
  ]
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration
  endpoint             = [for k, v in digitalocean_droplet.controlplane : v.ipv4_address][0]
  node                 = [for k, v in digitalocean_droplet.controlplane : v.ipv4_address][0]

  lifecycle {
    prevent_destroy = true
  }
}
