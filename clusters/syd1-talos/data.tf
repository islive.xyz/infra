data "talos_client_configuration" "talosconfig" {
  cluster_name         = local.cluster_name
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration
  endpoints            = [for k, v in digitalocean_droplet.controlplane : v.ipv4_address]
}

data "talos_cluster_kubeconfig" "kubeconfig" {
  depends_on = [
    talos_machine_bootstrap.bootstrap
  ]
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration
  endpoint             = [for k, v in digitalocean_droplet.controlplane : v.ipv4_address][0]
  node                 = [for k, v in digitalocean_droplet.controlplane : v.ipv4_address][0]
}

data "talos_machine_configuration" "controlplane" {
  cluster_name     = local.cluster_name
  cluster_endpoint = "https://${[for k, v in digitalocean_droplet.controlplane : v.ipv4_address][0]}:6443"

  machine_type    = "controlplane"
  machine_secrets = talos_machine_secrets.machine_secrets.machine_secrets

  talos_version      = local.talos_version
  kubernetes_version = local.kubernetes_version
}
