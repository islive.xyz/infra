terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.45"
    }
    talos = {
      source  = "siderolabs/talos"
      version = "0.5.0"
    }
    helm = {
      source = "hashicorp/helm"
    }
  }
}

locals {
  cluster_name              = "hcloud-fsn1-dc14-1"
  talos_install_disk        = "/dev/sda"
  cluster_domain            = "cluster.local"
  kubernetes_apiserver_fqdn = "kube-hcloud-fsn1-dc14-1.islive.xyz"
  kubernetes_version        = "v1.30.1"
  talos_version             = "v1.7.5"
  cilium_version            = "1.15.6"
  hcloud_ccm_version        = "v1.19.0"
  hcloud_csi_version        = "v2.5.1"
  talos_ccm_version         = "v1.6.0"
  api_port_kube_prism       = "7445"
  network_ipv4_cidr         = "10.0.0.0/16"
  node_ipv4_cidr            = "10.0.1.0/24"
  pod_ipv4_cidr             = "10.0.16.0/20"
  service_ipv4_cidr         = "10.0.8.0/21"
  server_count              = 3
  data_center               = "fsn1-dc14"
  location                  = "fsn1" # Frankfurt1
  server_type               = "cx32" # x86 4vcpu8gb

  # server_type               = "cax21" # ARM 4vcpu8gb
}

variable "hcloud_token" {}

data "hcloud_image" "x86" {
  with_selector = "os=talos,arch=x86"
  most_recent   = true
}

data "hcloud_datacenter" "this" {
  name = local.data_center
}

data "hcloud_location" "this" {
  id = data.hcloud_datacenter.this.location.id
}

resource "random_pet" "random" {
  count  = local.server_count
  length = 2
}

resource "tls_private_key" "ssh_key" {
  algorithm = "ED25519"
}

resource "hcloud_ssh_key" "this" {
  name       = "${local.cluster_name}-default"
  public_key = tls_private_key.ssh_key.public_key_openssh
  labels = {
    "cluster" = local.cluster_name
  }
}

resource "hcloud_placement_group" "control_plane" {
  name = "control-plane"
  type = "spread"
  labels = {
    "cluster" = local.cluster_name
  }
}

resource "hcloud_network" "this" {
  name     = local.cluster_name
  ip_range = local.network_ipv4_cidr
  labels = {
    "cluster" = local.cluster_name
  }
}

resource "hcloud_network_subnet" "nodes" {
  network_id   = hcloud_network.this.id
  type         = "cloud"
  network_zone = data.hcloud_location.this.network_zone
  ip_range     = local.node_ipv4_cidr
}

resource "hcloud_server" "cp" {
  for_each           = { for idx, val in random_pet.random : idx => val }
  name               = "server-${each.value.id}"
  server_type        = local.server_type
  ssh_keys           = [hcloud_ssh_key.this.id]
  image              = data.hcloud_image.x86.id
  placement_group_id = hcloud_placement_group.control_plane.id
  location           = local.location

  labels = {
    "cluster" = local.cluster_name,
    "role"    = "control-plane"
  }

  network {
    network_id = hcloud_network_subnet.nodes.network_id
    ip         = cidrhost(hcloud_network_subnet.nodes.ip_range, 100 + each.key)
    alias_ips  = [] # fix for https://github.com/hetznercloud/terraform-provider-hcloud/issues/650
  }

  lifecycle {
    prevent_destroy = true
  }
}

data "hcloud_floating_ip" "control_plane_ipv4" {
  id = hcloud_floating_ip.control_plane_ipv4.id
}

resource "hcloud_floating_ip" "control_plane_ipv4" {
  name              = "control-plane-ipv4"
  type              = "ipv4"
  home_location     = data.hcloud_location.this.name
  description       = "Control Plane VIP"
  delete_protection = false
  labels = {
    "cluster" = local.cluster_name,
    "role"    = "control-plane"
  }
}

resource "hcloud_floating_ip_assignment" "control_plane_ipv4" {
  floating_ip_id = data.hcloud_floating_ip.control_plane_ipv4.id
  server_id      = hcloud_server.cp[0].id
  depends_on = [
    hcloud_server.cp,
  ]
}

locals {
  talos_schematic     = jsondecode(data.http.talos_schematic.response_body).id
  talos_install_image = "factory.talos.dev/installer/${local.talos_schematic}:${local.talos_version}"
}
resource "talos_machine_secrets" "machine_secrets" {
  talos_version = local.talos_version
}
data "http" "talos_schematic" {
  url    = "https://factory.talos.dev/schematics"
  method = "POST"
  request_headers = {
    Accept       = "application/json"
    Content-type = "text/x-yaml"
  }
  request_body = <<-EOT
    customization:
        systemExtensions:
            officialExtensions:
                - siderolabs/iscsi-tools
                - siderolabs/util-linux-tools
                - siderolabs/binfmt-misc
                - siderolabs/kata-containers
   EOT
}
resource "talos_machine_configuration_apply" "cp" {
  for_each                    = { for idx, val in hcloud_server.cp : idx => val }
  endpoint                    = each.value.ipv4_address
  node                        = each.value.ipv4_address
  client_configuration        = talos_machine_secrets.machine_secrets.client_configuration
  machine_configuration_input = data.talos_machine_configuration.controlplane.machine_configuration
  depends_on                  = [hcloud_server.cp]
  config_patches = [
    <<-EOT
    machine:
       certSANs:
         - ${local.kubernetes_apiserver_fqdn}
         - ${hcloud_floating_ip.control_plane_ipv4.ip_address}
       kubelet:
         registerWithFQDN: true
         nodeIP:
           validSubnets:
             - ${each.value.ipv4_address}/32
         extraArgs:
           cloud-provider: external
           rotate-server-certificates: true
       features:
         kubePrism:
           enabled: true
           port: ${local.api_port_kube_prism}
         kubernetesTalosAPIAccess:
           enabled: true
           allowedRoles:
             - os:reader
           allowedKubernetesNamespaces:
             - kube-system
         hostDNS:
           enabled: true
           forwardKubeDNSToHost: true
           resolveMemberNames: true
       time:
         servers:
           - ntp1.hetzner.de
           - ntp2.hetzner.com
           - ntp3.hetzner.net
           - time.cloudflare.com
       install:
         disk: ${local.talos_install_disk}
         extraKernelArgs:
            - console=ttyS1,115200n8
            - talos.platform=hcloud
         wipe: false
         image: ${local.talos_install_image}
       network:
         kubespan:
           enabled: true
           advertiseKubernetesNetworks: false
           mtu: 1370
         hostname: ${each.value.name}
         interfaces:
           - interface: eth0
             dhcp: true
             vip:
               ip: ${hcloud_floating_ip.control_plane_ipv4.ip_address}
               hcloud:
                 apiToken: ${var.hcloud_token}
            # TODO private address vip?
    EOT
    ,
    <<-EOT
    cluster:
       allowSchedulingOnMasters: true
       proxy:
         disabled: true
       network:
         dnsDomain: ${local.cluster_domain}
         cni:
           name: none
         podSubnets:
           - ${local.pod_ipv4_cidr}
         serviceSubnets:
           - ${local.service_ipv4_cidr}
       externalCloudProvider:
         enabled: true
         manifests:
           - https://raw.githubusercontent.com/siderolabs/talos-cloud-controller-manager/${local.talos_ccm_version}/docs/deploy/cloud-controller-manager.yml
           - https://raw.githubusercontent.com/hetznercloud/csi-driver/${local.hcloud_csi_version}/deploy/kubernetes/hcloud-csi.yml
       controllerManager:
         extraArgs:
           cloud-provider: external
           node-cidr-mask-size-ipv4: ${split("/", local.node_ipv4_cidr)[1]}
           bind-address: "0.0.0.0"
       etcd:
         advertisedSubnets:
           - ${local.node_ipv4_cidr}
         extraArgs:
           listen-metrics-urls: "http://0.0.0.0:2381"
       scheduler:
         extraArgs:
           bind-address: "0.0.0.0"
       apiServer:
         extraArgs:
           cloud-provider: external
         certSANs:
           - ${local.kubernetes_apiserver_fqdn}
           - ${hcloud_floating_ip.control_plane_ipv4.ip_address}
       inlineManifests:
         - name: cilium
         - name: hcloud-ccm
         - name: hcloud-secret
           contents: |
             apiVersion: v1
             kind: Secret
             metadata:
               name: hcloud
               name: kube-system
             data:
               token: ${var.hcloud_token}
               network: ${local.cluster_name}
     EOT
    ,
    yamlencode([
      {
        "op" : "replace",
        "path" : "/cluster/inlineManifests/0/contents",
        "value" : data.helm_template.cilium.manifest
      },
      {
        "op" : "replace",
        "path" : "/cluster/inlineManifests/1/contents",
        "value" : data.helm_template.hcloud_ccm.manifest
      }
    ])
  ]
}

resource "talos_machine_bootstrap" "bootstrap" {
  depends_on = [
    talos_machine_configuration_apply.cp
  ]
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration
  endpoint             = [for k, v in hcloud_server.cp : v.ipv4_address][0]
  node                 = [for k, v in hcloud_server.cp : v.ipv4_address][0]
}


data "talos_client_configuration" "talosconfig" {
  cluster_name         = local.cluster_name
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration
  endpoints            = [for k, v in hcloud_server.cp : v.ipv4_address]
  nodes                = [for k, v in hcloud_server.cp : v.ipv4_address]
}

data "talos_cluster_kubeconfig" "kubeconfig" {
  depends_on = [
    talos_machine_bootstrap.bootstrap
  ]
  client_configuration = talos_machine_secrets.machine_secrets.client_configuration
  endpoint             = [for k, v in hcloud_server.cp : v.ipv4_address][0]
  node                 = [for k, v in hcloud_server.cp : v.ipv4_address][0]
}

data "talos_machine_configuration" "controlplane" {
  cluster_name     = local.cluster_name
  cluster_endpoint = "https://${local.kubernetes_apiserver_fqdn}:6443"

  machine_type    = "controlplane"
  machine_secrets = talos_machine_secrets.machine_secrets.machine_secrets

  talos_version      = local.talos_version
  kubernetes_version = local.kubernetes_version
}

data "helm_template" "cilium" {
  name      = "cilium"
  namespace = "kube-system"

  repository = "https://helm.cilium.io"
  chart      = "cilium"
  version    = local.cilium_version

  set {
    name  = "operator.replicas"
    value = local.server_count
  }
  set {
    name  = "ipam.mode"
    value = "kubernetes"
  }
  set {
    name  = "routingMode"
    value = "native"
  }
  set {
    name  = "ipv4NativeRoutingCIDR"
    value = local.pod_ipv4_cidr
  }
  set {
    name  = "kubeProxyReplacement"
    value = "true"
  }
  set {
    name  = "securityContext.capabilities.ciliumAgent"
    value = "{CHOWN,KILL,NET_ADMIN,NET_RAW,IPC_LOCK,SYS_ADMIN,SYS_RESOURCE,DAC_OVERRIDE,FOWNER,SETGID,SETUID}"
  }
  set {
    name  = "securityContext.capabilities.cleanCiliumState"
    value = "{NET_ADMIN,SYS_ADMIN,SYS_RESOURCE}"
  }
  set {
    name  = "cgroup.autoMount.enabled"
    value = "false"
  }
  set {
    name  = "cgroup.hostRoot"
    value = "/sys/fs/cgroup"
  }
  set {
    name  = "k8sServiceHost"
    value = "127.0.0.1"
  }
  set {
    name  = "k8sServicePort"
    value = local.api_port_kube_prism
  }
  set {
    name  = "hubble.enabled"
    value = "false"
  }
}

data "helm_template" "hcloud_ccm" {
  name      = "hcloud-cloud-controller-manager"
  namespace = "kube-system"

  repository = "https://charts.hetzner.cloud"
  chart      = "hcloud-cloud-controller-manager"
  version    = local.hcloud_ccm_version

  set {
    name  = "networking.enabled"
    value = "true"
  }

  set {
    name  = "networking.clusterCIDR"
    value = local.pod_ipv4_cidr
  }

  set {
    name  = "env.HCLOUD_LOAD_BALANCERS_LOCATION.value"
    value = data.hcloud_location.this.name
  }
}

output "talosconfig" {
  value     = data.talos_client_configuration.talosconfig.talos_config
  sensitive = true
}

output "kubeconfig" {
  value     = data.talos_cluster_kubeconfig.kubeconfig.kubeconfig_raw
  sensitive = true
}

output "control_plane_ipv4" {
  value = hcloud_floating_ip.control_plane_ipv4.ip_address
}

output "hcloud_ccm" {
  sensitive = true
  value     = data.helm_template.hcloud_ccm.manifest
}

output "talos_install_image" {
  value = local.talos_install_image
}
