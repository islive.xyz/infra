terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}

variable "scale" {
  type = number
}

data "digitalocean_kubernetes_versions" "version" {
  version_prefix = "1.30."
}

resource "digitalocean_kubernetes_cluster" "syd1" {
  name                             = "syd1"
  region                           = "syd1"
  surge_upgrade                    = true
  auto_upgrade                     = true
  destroy_all_associated_resources = false
  registry_integration             = false
  version                          = data.digitalocean_kubernetes_versions.version.latest_version

  maintenance_policy {
    start_time = "04:00"
    day        = "any"
  }

  node_pool {
    name       = "small-1"
    size       = "s-1vcpu-2gb"
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 1
  }

  count = var.scale
}
