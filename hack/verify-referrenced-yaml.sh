#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

NOT_FOUND=""
for FILE in $( \
                 find . \
                 -mindepth 1 \
                 -type f \
                 -name '*.yaml' \
                 -not -path '*/\.git/*' \
                 -not -path './dns/*.yaml' \
                 -not -name 'kustomization.yaml' \
                 -not -path './clusters/*/*.yaml' \
                 -not -path './packer/*' \
                 -not -path './.terraform/*' \
                 -not -name '.conform.yaml' \
                 ); do
    FOUND=false
    BASENAME="$(basename "$FILE")"
    if grep -q "\# ignore:unreferrenced" "$FILE"; then
        continue
    fi
    for CONTENT in $(find . -type f -name '*.yaml'); do
        if grep -q "$BASENAME" "$CONTENT"; then
            FOUND=true
        fi
    done
    if [ ! "$FOUND" = true ]; then
        NOT_FOUND="$NOT_FOUND $FILE"
    fi
done

if [ -n "$NOT_FOUND" ]; then
    for NF in $NOT_FOUND; do
        echo "$NF"
    done
    exit 1
fi
