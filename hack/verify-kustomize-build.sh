#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

if ! find \
    ./apps ./infrastructure/configs ./infrastructure/controllers \
    -mindepth 1 -maxdepth 1 \
    -name 'kustomization.yaml' | \
    xargs -I{} dirname {} | \
    xargs -I{} kustomize build {} -o /dev/null; then
    echo
    echo "failed to build kustomization"
    exit 1
fi
