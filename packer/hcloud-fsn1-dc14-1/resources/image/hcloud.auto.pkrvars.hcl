

# #+RESULTS: schematic-id
# #+begin_example
# 655b46b633e7402587974e35b5f7b440adab34f5f2d286f80adc41416af0143a
# #+end_example

# set default values


talos_version = "v1.7.5"
image_url_arm = "https://factory.talos.dev/image/655b46b633e7402587974e35b5f7b440adab34f5f2d286f80adc41416af0143a/v1.7.5/hcloud-arm64.raw.xz"
image_url_x86 = "https://factory.talos.dev/image/655b46b633e7402587974e35b5f7b440adab34f5f2d286f80adc41416af0143a/v1.7.5/hcloud-amd64.raw.xz"
