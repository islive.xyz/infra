output "cluster-hcloud-fsn1-dc14-1_talosconfig" {
  value     = module.cluster-hcloud-fsn1-dc14-1.talosconfig
  sensitive = true
}

output "cluster-hcloud-fsn1-dc14-1_kubeconfig" {
  value     = module.cluster-hcloud-fsn1-dc14-1.kubeconfig
  sensitive = true
}

output "cluster-hcloud-fsn1-dc14-1_control_plane_ipv4" {
  value = module.cluster-hcloud-fsn1-dc14-1.control_plane_ipv4
}
